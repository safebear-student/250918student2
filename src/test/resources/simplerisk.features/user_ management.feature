Feature: User Management

  User Story:
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
  - Non-admin users have no permissions over other accounts
  - There are two types of non-admin accounts
  -- Risk managers
  -- Asset managers

  Questions:
  - Do admin users need access to all accounts or only to the acounts in their group?

  To do:
  - Force reset of password on account creation

  Domain language:
  Group = Users are defined by which Group they belong to
  CRUD = Create, Read, Update, Delete

  Background:
    Given a user called simon with administrator permissions and password S@feB3ar
    And a user called Tom with Risk_management permissions and password S@feB3ar

  @high-impact
  Scenario Outline: The admin checks a user's details
    When simon is logged in with password S@feB3ar
    Then he is able to view <user>'s account
    Examples:
      | user |
      | Tom  |

  @high-risk
  @to-do
  Scenario Outline: A user's password is reset
  by the admin
    Given a <user> has lost his password
    When simon is logged in with password S@feB3ar
    Then simon can reset <user>'s password
    Examples:
      | user |
      | Tom  |



