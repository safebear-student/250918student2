package com.bdd.app;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by CCA_Student on 25/09/2018.
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:target/cucumber"},
        tags = "~@to-do",
        glue = "com.bdd.app",
        features = "classpath:simplerisk.features/user_ management.feature"
)

public class RunCukesTest {
}
